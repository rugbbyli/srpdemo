﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "SRPDemo/Transparent/Lighting" {
    Properties{
        _Diffuse("Diffuse", 2D) = "white" {}
        _Specular("Specular", 2D) = "white" {}
        _Shininess("Shininess", Float) = 10
    }
        SubShader
    {
        Pass
    {
        Tags{ "LightingMode" = "SRPDemoLighting" "RenderType"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
        Cull Off
        CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"
#include "Lighting.cginc"
#include "AutoLight.cginc"
        struct appdata
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
        float3 normal : NORMAL;
    };

    struct v2f
    {
        float2 uv : TEXCOORD0;
        float4 vertex : SV_POSITION;
        float3 normalDir : NORMAL;
        float3 worldPos : TEXCOORD1;
    };

    v2f vert(appdata v)
    {
        v2f o;
        o.uv = v.uv;
        o.vertex = UnityObjectToClipPos(v.vertex);
        o.normalDir = normalize(mul(v.normal, (float3x3)unity_WorldToObject));
        o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
        return o;
    }

    sampler2D _Diffuse;
    sampler2D _Specular;
    float _Shininess;
    float _Cutoff;

    fixed4 frag(v2f i) : SV_Target
    {
        // 法线方向
        float3 normDir = normalize(i.normalDir);
        // 灯光方向
        float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
        // 灯光颜色
        float3 lightColor = _LightColor0.rgb;

        float4 diffuseTex = tex2D(_Diffuse, i.uv);
        float3 specTex = tex2D(_Specular, i.uv).rgb;

        //环境光
        float3 envColor = UNITY_LIGHTMODEL_AMBIENT.xyz * diffuseTex * 0.2f;

        //Diffuse
        float3 diffuseColor = lightColor * saturate(dot(normDir, lightDir)) * diffuseTex.rgb;

        //Specular
        float3 viewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
        float3 reflectDir = normalize(reflect(-lightDir, normDir));
        float spec = pow(saturate(dot(viewDir, reflectDir)), _Shininess);
        float3 specColor = 1 * spec * lightColor * specTex;

        float3 finalColor = (envColor + diffuseColor + specColor);

        return fixed4(finalColor, diffuseTex.a);
    }
        ENDCG
    }
    }
}