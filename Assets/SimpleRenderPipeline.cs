﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;

public class SimpleRenderPipeline : RenderPipeline
{
    public override void Render(ScriptableRenderContext renderContext, Camera[] cameras)
    {
        base.Render(renderContext, cameras);

        BeginFrameRendering(cameras);

        foreach (var camera in cameras)
        {
            // Culling
            CullResults cull;
            if(!CullResults.Cull(camera, renderContext, out cull))
            {
                continue;
            }

            // Setup camera for rendering (sets render target, view/projection matrices and other
            // per-camera built-in shader variables).
            renderContext.SetupCameraProperties(camera);

            // clear buffer
            var cmd = new CommandBuffer();
            cmd.ClearRenderTarget(true, true, camera.backgroundColor);
            renderContext.ExecuteCommandBuffer(cmd);
            cmd.Release();

            SetLightingVariables(ref renderContext, ref cull);

            DrawOpaque(camera, ref renderContext, ref cull.visibleRenderers);
            
            // Draw skybox
            renderContext.DrawSkybox(camera);

            DrawTransparent(camera, ref renderContext, ref cull.visibleRenderers);
            
            renderContext.Submit();
        }

        renderContext.Submit();
    }
   
    void DrawOpaque(Camera camera, ref ScriptableRenderContext context, ref FilterResults visibleRenderers)
    {
        var filterSettings = new FilterRenderersSettings(true) { renderQueueRange = RenderQueueRange.opaque };
        var drawRenderSettings = CreateDrawRendererSettings(camera, SortFlags.CommonOpaque);
        context.DrawRenderers(visibleRenderers, ref drawRenderSettings, filterSettings);
    }

    void DrawTransparent(Camera camera, ref ScriptableRenderContext context, ref FilterResults visibleRenderers)
    {
        var filterSettings = new FilterRenderersSettings(true) { renderQueueRange = RenderQueueRange.transparent };
        var drawRenderSettings = CreateDrawRendererSettings(camera, SortFlags.CommonTransparent);
        context.DrawRenderers(visibleRenderers, ref drawRenderSettings, filterSettings);
    }

    void SetLightingVariables(ref ScriptableRenderContext context, ref CullResults cullResults)
    {

        VisibleLight? vlight = null;
        //find main light
        foreach (var light in cullResults.visibleLights)
        {
            if (light.lightType == LightType.Directional)
            {
                vlight = light;
                break;
            }
        }
        //if (vlight == null && cullResults.visibleLights.Count > 0)
        //    vlight = cullResults.visibleLights[0];

        if(vlight != null)
        {
            CommandBuffer buf = new CommandBuffer();

            var pos_tag = Shader.PropertyToID("_WorldSpaceLightPos0");
            var color_tag = Shader.PropertyToID("_LightColor0");

            var dir = -vlight.Value.localToWorld.GetColumn(2);
            buf.SetGlobalVector(pos_tag, new Vector4(dir.x, dir.y, dir.z, 0f));

            buf.SetGlobalVector(color_tag, vlight.Value.finalColor);
            context.ExecuteCommandBuffer(buf);

            buf.Release();
        }

    }

    private DrawRendererSettings CreateDrawRendererSettings(Camera camera, SortFlags sortFlags)
    {
        var drs = new DrawRendererSettings(camera, new ShaderPassName("SRPDefaultUnlit"));
        //drs.SetShaderPassName(1, new ShaderPassName("SRPDemoLighting"));
        drs.sorting.flags = sortFlags;

        drs.rendererConfiguration = RendererConfiguration.PerObjectLightProbe | RendererConfiguration.PerObjectLightmaps;
        drs.flags = DrawRendererFlags.EnableDynamicBatching;

        return drs;
    }









    private SimpleRenderPipelineAsset pipelineAsset;
    public SimpleRenderPipeline(SimpleRenderPipelineAsset asset)
    {
        pipelineAsset = asset;
    }
}
