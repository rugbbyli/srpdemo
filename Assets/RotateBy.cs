﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBy : MonoBehaviour {

    public Transform target;
    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(target != null)
        {
            this.transform.LookAt(target);
            var hinput = Input.GetAxis("Horizontal");

            this.transform.RotateAround(target.position, Vector3.up, hinput * speed * Time.deltaTime);
        }
	}
}
